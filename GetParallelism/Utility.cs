﻿using System;
using SolidWorks.Interop.sldworks;

namespace Sandbox
{
    public enum GetParallelismResult
    {
        Parallel,
        NotParallel,
        FirstEntityNotPlanarOrNull,
        SecondEntityNotPlanarOrNull,
        BothEntitiesNotPlanarOrNull,
    }

    class Utility
    {
        const int tolerancePct = 1;
        const int decimals = 6;

        /// <summary>
        /// Returns whether two objects are parallel and, if they are, their distance.
        /// </summary>
        /// <param name="mathUtil"></param>
        /// <param name="obj1">Accepts IFeature, ISketch, IRefPlane, or IFace2.</param>
        /// <param name="obj2">Accepts IFeature, ISketch, IRefPlane, or IFace2.</param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static GetParallelismResult GetParallelism(MathUtility mathUtil, object obj1, object obj2, out double distance)
        {
            distance = Double.NaN;

            if (obj1 == null & obj2 == null)
                return GetParallelismResult.BothEntitiesNotPlanarOrNull;
            if (obj1 == null)
                return GetParallelismResult.FirstEntityNotPlanarOrNull;
            if (obj2 == null)
                return GetParallelismResult.SecondEntityNotPlanarOrNull;

            object planarEnt1 = null;
            object planarEnt2 = null;
            double[] data1 = GetPlaneData(obj1, ref planarEnt1);
            double[] data2 = GetPlaneData(obj2, ref planarEnt2);

            if (data1 == null & data2 == null)
                return GetParallelismResult.BothEntitiesNotPlanarOrNull;
            if (data1 == null)
                return GetParallelismResult.FirstEntityNotPlanarOrNull;
            if (data2 == null)
                return GetParallelismResult.SecondEntityNotPlanarOrNull;

            double[] transformedData1 = TransformDataToAssemblySpace(mathUtil, obj1, data1);
            double[] transformedData2 = TransformDataToAssemblySpace(mathUtil, obj2, data2);

            if (transformedData1 != null)
                data1 = transformedData1;
            if (transformedData2 != null)
                data2 = transformedData2;

            if (IsWithinTolerance(Math.Abs(data1[0]), Math.Abs(data2[0]), tolerancePct, decimals) &
                IsWithinTolerance(Math.Abs(data1[1]), Math.Abs(data2[1]), tolerancePct, decimals) &
                IsWithinTolerance(Math.Abs(data1[2]), Math.Abs(data2[2]), tolerancePct, decimals))
            {
                // Calculate the distance using this formula:
                // D = hypotenuse * cos(angle)
                // hypotenuse = distance between any point on first plane and any point on second plane
                // angle = arccos of the dot products of the normal unit vectors of one of the planes and
                // hypotenuse unit vector
                // Note that arccos and cos cancel each other out, which is why they aren't included below.
                double hyp = Math.Sqrt(Math.Pow(data2[3] - data1[3], 2) +
                                       Math.Pow(data2[4] - data1[4], 2) +
                                       Math.Pow(data2[5] - data1[5], 2));
                double[] hypVector = new double[3] { data2[3] - data1[3], data2[4] - data1[4], data2[5] - data1[5] };
                double[] hypUnitVector = new double[3] { hypVector[0] / hyp, hypVector[1] / hyp, hypVector[2] / hyp };
                double dotProduct = data1[0] * hypUnitVector[0] + data1[1] * hypUnitVector[1] + data1[2] * hypUnitVector[2];
                distance = hyp * dotProduct;
                if (double.IsNaN(distance))         // If planes are coincident
                    distance = 0;
                return GetParallelismResult.Parallel;
            }
            else
                return GetParallelismResult.NotParallel;
        }

        /// <summary>
        /// Get the normal and root point data for planes and planar faces.
        /// </summary>
        /// <param name="entity">Accepts IFeature, IRefPlane, ISketch, and IFace2.</param>
        /// <returns>Array of 6 doubles. First three are normal and second three are root point.</returns>
        static double[] GetPlaneData(object entity, ref object planarEntity)
        {
            int retVal = 0;
            
            while (true)
            {
                switch (entity)
                {
                    case RefPlane refPlane:
                        planarEntity = entity;
                        double[] arrData = (double[])refPlane.Transform.ArrayData;
                        // Elements 7-9 (z direction vector) are plane normal.
                        return new double[6] { arrData[6], arrData[7], arrData[8], arrData[9], arrData[10], arrData[11] };
                    case Sketch sketch:
                        if (sketch.Is3D())
                            return null;
                        entity = sketch.GetReferenceEntity(ref retVal);
                        break;
                    case Feature feat when (feat.GetSpecificFeature2() is Sketch):
                        entity = (Sketch)feat.GetSpecificFeature2();
                        break;
                    case Feature feat when (feat.GetSpecificFeature2() is RefPlane):
                        entity = (RefPlane)feat.GetSpecificFeature2();
                        break;
                    case Face2 face:
                        planarEntity = face;
                        // Find an arbitrary point on face. Also tried IFace2::GetClosestPointOn and 
                        // IFace2::GetTessTriangles but they did not return pts on the face, apparently?
                        Surface surf = (Surface)face.GetSurface();
                        return (double[])surf.PlaneParams;
                    default:
                        return null;
                }
            }
        }

        static double[] TransformDataToAssemblySpace(MathUtility mathUtil, object obj, double[] data)
        {
            Component2 comp = GetObjectsComponent(obj);
            if (comp == null)
                return null;

            MathVector mathVect = mathUtil.CreateVector(new double[3] { data[0], data[1], data[2] });
            MathPoint mathPt = mathUtil.CreatePoint(new double[3] { data[3], data[4], data[5] });
            MathTransform transform = comp.Transform2;
            mathVect = mathVect.MultiplyTransform(transform);
            mathPt = mathPt.MultiplyTransform(transform);

            return new double[] { mathVect.ArrayData[0], mathVect.ArrayData[1], mathVect.ArrayData[2],
                                  mathPt.ArrayData[0], mathPt.ArrayData[1], mathPt.ArrayData[2] };
        }

        static Component2 GetObjectsComponent(object obj)
        {
            Entity ent = (Entity)obj;
            Component2 comp = (Component2)ent.GetComponent();
            string compName = comp.Name2; // debug purposes only
            return comp;
        }

        static bool IsWithinTolerance(double value1, double value2, double tolerancePct, int decimals)
        {
            double absTolerance = Math.Round(Math.Abs(value1) * tolerancePct * 0.01, decimals);
            double difference = Math.Round(Math.Abs(value1 - value2), decimals);

            if (difference <= absTolerance)
                return true;
            else
                return false;
        }
    }
}
