﻿using System;
using System.Collections.Generic;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swconst;

namespace Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            SldWorks app = GetSolidWorks(2018);
            if (app != null)
                app.Visible = true;
            else
            {
                Console.WriteLine("Failed to get SolidWorks pointer.");
                Console.ReadKey();
                return;
            }

            ModelDoc2 model = app.ActiveDoc;
            if (model == null || model.GetType() == (int)swDocumentTypes_e.swDocDRAWING)
            {
                Console.WriteLine("Failed to get active part or assembly.");
                Console.ReadKey();
                return;
            }

            double distance;

            // TEST WITH ONE OF THE FOLLOWING METHODS: TestWithSelections() or TestWithTraversal()
            // Note that TestWithTraversal() is limited to testing features. Faces cannot be tested.

            GetParallelismResult result = TestWithSelections(app, model, out distance);
            //GetParallelismResult result = TestWithTraversal(app, model, new List<string> { "Sketch3", "PLANE2" }, out distance);

            Console.WriteLine(result.ToString());
            if (result == GetParallelismResult.Parallel)
                Console.WriteLine(Math.Round(distance, 6).ToString() + "m");
            Console.ReadKey();
        }

        static GetParallelismResult TestWithSelections(SldWorks app, ModelDoc2 model, out double distance)
        {
            distance = -1;

            SelectionMgr selMgr = model.SelectionManager;

            if (selMgr.GetSelectedObjectCount2(-1) != 2)
            {
                Console.WriteLine("Wrong number of selections.");
                Console.ReadKey();
                return (GetParallelismResult)(-1);
            }

            object obj1 = selMgr.GetSelectedObject6(1, -1);
            object obj2 = selMgr.GetSelectedObject6(2, -1);

            return Utility.GetParallelism(app.GetMathUtility(), obj1, obj2, out distance);
        }

        static GetParallelismResult TestWithTraversal(SldWorks app, ModelDoc2 model, 
                                                        List<string> featNames, out double distance)
        {
            distance = -1;

            // Specify two feature names. If these names are not unique in the entire assembly then the
            // wrong feature could be found because this method will get the first method with a matching name.
            List<object> objs = GetObjectsByTraversal(featNames, model);

            if (objs.Count < 2)
            {
                Console.WriteLine("Could not find specified features.");
                Console.ReadKey();
                return (GetParallelismResult)(-1);
            }

            return Utility.GetParallelism(app.GetMathUtility(), objs[0], objs[1], out distance);
        }

        static List<object> GetObjectsByTraversal(List<string> featNames, ModelDoc2 model)
        {
            List<object> pointers = new List<object>();

            TraverseModel(model, featNames, ref pointers);

            return pointers;
        }

        static void TraverseModel(object model, List<string> featNames, ref List<object> pointers)
        {
            Feature feat = null;
            ModelDoc2 modelDoc = null;
            Component2 compDoc = null;

            if (model is ModelDoc2)
            {
                modelDoc = (ModelDoc2)model;
                feat = modelDoc.FirstFeature();
            }

            if (model is Component2)
            {
                compDoc = (Component2)model;
                feat = compDoc.FirstFeature();
            }

            while (feat != null)
            {
                if (featNames.Contains(feat.Name))
                    pointers.Add(feat);

                if (pointers.Count == 2)
                    return;

                if (feat.GetTypeName() == "Reference")
                {
                    Component2 comp = feat.GetSpecificFeature2();
                    TraverseModel(comp, featNames, ref pointers);
                }

                feat = feat.GetNextFeature();
            }
        }

        static SldWorks GetSolidWorks(int year)
        {
            int version = year - 1992;
            if (version < 1)
                return null;

            Type type = Type.GetTypeFromProgID("SldWorks.Application." + version.ToString());
            if (type == null)
                return null;

            return (SldWorks)Activator.CreateInstance(type);
        }
    }
}
