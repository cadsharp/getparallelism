# README #

GetParallelism is a function for finding whether two planar entities (i.e., reference planes and faces) are parallel and the distance between them. While this information can be obtained via Measure, it is sometimes helpful to avoid the tedium related to selections involved with that object.

**Language**

C# 7.0, available in Visual Studio 2017.

**References**

SolidWorks.Interop.sldworks.dll

**Arguments**

*mathUtil* - Pointer to MathUtility object.

*obj1* - Pointer to Face2, Sketch, RefPlane, or Feature (must correspond to Sketch or RefPlane).

*obj2* - Same as previous.

*distance* (out) - The distance in meters between the two objects. Returns NaN if planes are not parallel.

**Return Value**

Whether entities are parallel or whether inputs were valid.

```cs
public enum GetParallelismResult
{
    Parallel,
    NotParallel,
    FirstEntityNotPlanarOrNull,
    SecondEntityNotPlanarOrNull,
    BothEntitiesNotPlanarOrNull,
}
```

**Known Limitations**

* Sketches created in context will not return correct distance values. This is due to a bug in Entity.GetComponent(), documented here: http://www.evernote.com/l/APAwdRP6Bm9JLry4xMkBaK3DcveAsTED1g4/.

**Design Notes**

* The vector algebra calculations in GetParallelism() could use IMathUtility. We opted not to because you cannot see the values stored within SolidWorks API objects.
* GetPlaneData() is the function that uses C# 7.0 features: "when" keyword and using switch to test for data types.
* GetPlaneData() uses a while loop to greatly reduce the amount of code necessary. For example, if a sketch's IFeature pointer is passed in, then this function will first obtain the underlying ISketch pointer and then the sketch's reference entity, either IFace2 or IRefPlane. From either of those objects, a normal and arbitrary point can be obtained.
* TransformDataToAssemblySpace() and GetObjectsComponent() are necessary for finding parallelism / distance in assemblies. Therefore, if this code was intended for use in parts only, these functions could be removed with slight adjustment to GetParallelism().